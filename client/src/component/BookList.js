import React, { Component } from 'react';
import {graphql} from 'react-apollo';
import {getBooksQuery} from '../queries/queries'
import BookDetails from './Bookdetails';


class Booklist extends Component {
  constructor(props){
    super(props)
      this.state={
        selected : null
    }
  }
  displayBooks(){
      let data = this.props.data;
      if(data.loading){
          return(<div>Loading books...</div>)
      }else {
        return data.books.map(book=>{
            return(
                <li key={book.id} id='book-list-li' onClick={(e)=>{this.setState({selected:book.id})}}>{book.name}</li>
            )
        })
      }
  }
  render() {      
    return (
      <div >
          <ul id="book-list">
          {this.displayBooks()}
          </ul>
          <BookDetails bookid={this.state.selected}/>
      </div>
    );
  }
}

export default graphql(getBooksQuery)(Booklist);
 