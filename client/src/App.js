import React, { Component } from 'react';
import ApolloClient from 'apollo-boost'
import {ApolloProvider} from 'react-apollo'
import './index.css'; 

//components import 
import Booklist from './component/BookList'
import AddBook from './component/AddBook';

//apollo client setup 

const client = new ApolloClient({
  uri:"http://localhost:4000/graphql"
})

class App extends Component {
  render() {
    return (

      <ApolloProvider client={client}>
      <div id="main">
        <h1>List</h1>
        <Booklist/>
        <AddBook/>
      </div>
      </ApolloProvider>
    );
  }
}

export default App;
