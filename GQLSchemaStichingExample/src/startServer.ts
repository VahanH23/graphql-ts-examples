// import "reflect-metadata";
import {importSchema } from 'graphql-import';
// import { makeExecutableSchema } from 'graphql-tools'
import { GraphQLServer } from 'graphql-yoga';
// import {resolvers} from './resolvers'
import { createTypeOrmConn } from "./utils/createTypeormConn";
import  * as fs from "fs"
import  * as path from 'path'
import {
    makeExecutableSchema,
    mergeSchemas,
  } from 'graphql-tools';
import { GraphQLSchema } from 'graphql';
  // const typeDefs = importSchema("schema.graphql")
// const typeDefs = importSchema(path.join(_dirname,"./schema.graphql"));


export const startServer = async ()=> {
  const schemas:GraphQLSchema[] = [];
  const folders = fs.readdirSync(path.join(__dirname,"./modules")) 
  folders.forEach((folder)=>{
      const {resolvers} = require(`./modules/${folder}/resolvers`)
      const typeDefs = importSchema(path.join(__dirname,`./modules/${folder}/schema.graphql`)
      );
      schemas.push(makeExecutableSchema({resolvers,typeDefs}))
  });
  const server = new GraphQLServer({schema:mergeSchemas({schemas}) })

  await createTypeOrmConn()
  const app = await server.start({
    port : process.env.NODE_ENV === "test" ? 0 : 4000
  })
  
  console.log('Serever is running on licalhost : 4000');

  return app
}