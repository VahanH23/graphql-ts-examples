const app = require('express')();
const graphqlHTTP = require('express-graphql');
const schema = require('./schema/schema');
// const {buildSchema} = require('graphql')
const mongoose = require('mongoose');
const cors = require('cors')


//allow cross-origin request
app.use(cors())

mongoose.connect('mongodb://localhost:27017/mydb', {useNewUrlParser: true});

mongoose.connection.once('open',()=>{
    console.log('connected to database ');
})



app.use('/graphql',graphqlHTTP({
    //same as schema : schema
    //graphql schema        
     schema,
     graphiql: true,
     
}))
app.listen(4000,()=> console.log('listening'));