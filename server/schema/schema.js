import _ from 'lodash';
import Book from '../models/book';
import Author from '../models/author';
import {GraphQLObjectType,
    GraphQLString,
    GraphQLSchema,
    GraphQLID,
    GraphQLInt,     
    GraphQLList, 
    GraphQLNonNull} from 'graphql';

const AuthorType = new GraphQLObjectType ( {
    name : "Author",
    fields: ()=>({
        id : {type:GraphQLID},
        name: {type:GraphQLString},
        age:{type:GraphQLInt},
      
        books : {            
            type : new GraphQLList(BookType),
            resolve(parent,args){
                console.log(parent);
                // return _.filter(books,{authorId:parent.id})
                return Book.find({authorId : parent.id});
            }
        }

    })
});
const BookType = new GraphQLObjectType ( {
    name : "Book",
    fields: ()=>({
        id : {type:GraphQLID},
        name: {type:GraphQLString},
        genre:{type:GraphQLString},
    
        author:{
            type:AuthorType,
            resolve(parent,args){
                console.log(parent); 
                console.log(parent.authorId);
                return Author.findById(parent.authorId)
                
            }
        }
    })
});

const GameType = new GraphQLObjectType ( {
    name : "Game",
    fields: ()=>({
        id : {type:GraphQLID},
        name: {type:GraphQLString},
        genre:{type:GraphQLString},
    })
});


const RootQuery = new GraphQLObjectType({
    name : 'RootQueryType',
    fields:{
        book:{           
            type:BookType,
            args:{id:{type:GraphQLID}},
            resolve(parent,args){
                console.log(typeof(args.id));
                return Book.findById(args.id);
                // return  _.find(books,{id:args.id})
            }
        },
        author:{
            type:AuthorType,
            args:{id:{type:GraphQLID}},
            resolve(parent,args){
                return Author.findById(args.id);
                // return _.find(authors,{id:args.id})
            }
        },
        game:{
            type:GameType,
            args:{name:{type:GraphQLString}},
            resolve(parent,args){
                // return _.find(games,{name:args.name})
            }
        },
        books:{
            type : new GraphQLList(BookType),
            resolve(parent,args){
                //ete datark obj-a sax veradarcnuma
                return Book.find({})
                // return books
            }
        },
        authors : {
            type : new GraphQLList(AuthorType),
            resolve(parent,args){
                return Author.find({})
                // return authors
            }
        }

    }
}) 
//this insert in db 
const Mutation =new GraphQLObjectType({
    name : "Mutation",
    fields: {
        addAuthor:{
            type : AuthorType,
            args : {
                name : {type:GraphQLString},
                age : {type : GraphQLInt}, 
                // id : {type :GraphQLID}
            },
            resolve(parent,args){
                let author = new Author({
                    name : args.name,
                    age : args.age,  
                    // id : args.id 
                }); 
                return author.save()
            }
        },
        
        addBook : {
            type : BookType,
            args : {
                name : {type :new GraphQLNonNull(GraphQLString)},
                genre : {type:new GraphQLNonNull (GraphQLString)},
                authorId : {type : GraphQLID},
            },
                resolve(parent,args){
                    //we use new beacuse we import Book
                    let book = new Book({
                        name : args.name,
                        genre : args.genre,
                        authorId : args.authorId,
                    });
                    return book.save()
                }
            
        }
    },

})


module.exports = new GraphQLSchema({
    query : RootQuery ,
    mutation : Mutation 
})

